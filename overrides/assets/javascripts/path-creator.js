var paths = document.getElementById("paths");
const padding = 4;

var bezierWeight = 0.675;

var connections = [{
    boxA: "#VA",
    boxB: "#Sched"
}, {
    boxA: "#GA",
    boxB: "#Sched"
}, {
    boxA: "#RAVEN",
    boxB: "#Sched"
}
];

const coordinates = function () {

    let oldPaths = paths.children;

    for (let a = oldPaths.length - 1; a >= 0; a--) {
        paths.removeChild(oldPaths[a]);
    }

    let x1, y1, x4, y4, dx, x2, x3, path, boxA, boxB;

    for (let a = 0; a < connections.length; a++) {
        boxA = $(connections[a].boxA);
        boxB = $(connections[a].boxB);

        x1 = boxA.offset().left + boxA.width() / 2;
        y1 = boxA.offset().top + boxA.height() / 2;
        x4 = boxB.offset().left + boxB.width() / 2;
        y4 = boxB.offset().top + boxB.height() / 2;

        let width = screen.width;

        if (width > 800) {
            data = `
                M${x1} ${y1}
                L${x1} ${y4}
            `
            // only straight lines up and down thus x1 used for both
        }
        else {
            if (connections[a].boxA == "#VA") {
                data = `
                    M${x4} ${y1}
                    L${x4} ${y4}
                `
            }
            else {
                data = `
                    M${x1} ${y1}
                    L${x4} ${y1}
                `
            }
        }


        path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        path.setAttribute("d", data);
        path.setAttribute("class", "path");
        paths.appendChild(path);
    }
}

$(window).load(function () {
    coordinates();
})

$(window).resize(function () {
    coordinates();
});