---
Title: Legal notice
---

# Virtual Acoustics website legal notice {style="text-align: center;"}

The Institute for Hearing Technology and Acoustics (IHTA) at RWTH Aachen University is responsible for the contents of this website
{style="text-align: center; font-size:1.2em"}
<hr class="accent-hr">

## Contact
RWTH Aachen University<br>
Institute for Hearing Technology and Acoustics, RWTH Aachen University<br>
Kopernikusstraße 5<br>
D-52074 Aachen, Germany

For more information visit the [official website](https://www.akustik.rwth-aachen.de/){target="_blank"} of the institute.

## Virtual Acoustics website

This side was made using [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/){target="_blank"}.<br>
It is licensed under the MIT License while [MkDocs](https://www.mkdocs.org/){target="_blank"} itself is licensed under the BSD 2-Clause "Simplified" License.<br>
The color palette is inspired by a free template called [Landed by HTML 5UP](https://html5up.net/landed){target="_blank"}.

## Licensing

### Code
The source code for [Virtual Acoustics](VA/index.md) and [ITAGeometricalAcoustics](GA/index.md) projcets is licensed under [Apache License Version 2.0](https://www.apache.org/licenses/LICENSE-2.0){target="_blank"}.

### Resources
[Virtual Acoustics](VA/index.md) provides some input data necessary for auralization - like an HRTF dataset. Resources are generally licensed under the original [Creative Commons Version 4.0](https://creativecommons.org/){target="_blank"} license or a CC derivative that may include share-alike distribution or prohibits commercial use. The individual license for each work is usually placed next to the resource file and is typically called LICENSE. Some files may also have the license embedded in the metadata, if possible.

### External libraries
Our software makes use of third party libraries. If you are planning to create and share an application based on any software on this website, you will have to pay close attention to the individual licenses of those projects. Especially GPL licensed third party software and static linking of third party libraries licensed under LGPL will require to share (at least) parts of your project. Other third party libraries, like fftw3, may be purchased to legally include it in your application. In any case, make yourself familiar with the individual license agreements.
> Please respect the license agreements of every individual project you are planning to depend on. We will not be hold responsible if this concept is misused.

### Binary packages
For binary packages, please check the licensing files within the respective root folder (typically called *LICENSE* or *COPYING*). As our software depends on third party libraries, the license might vary between different software packages or even different build configurations of the same software.
