---
Title: Research
---

# Research section {style="text-align: center;"}

Publications that are related to the Virtual Acoustics (VA) framework
{style="font-size:1.2em;text-align: center;"}
<hr class="accent-hr">

## Preface
Virtual Acoustics is used by the Institute for Hearing Technology and Acoustics for listening experiments and Virtual Reality applications. As a result, a lot of publications are related to VA. Find the most relevant papers of IHTA below, ordered by [system](#va-system-publications), [technology](#va-technology-publications) and [applied](#applied-va-publications) sections.

If you are also using VA and want your publications to be listed as an applied VA publication, please [contact](../legal.md) us.

Naturally, VA has implemented a lot of methods that are available by publications of others. Please understand that we can not give credit to everyone, but if you inspect the source code you will find related publications where appropriate.

A technical report is currently in progress. If you want to use VA for your research and would like refer to it in a scientific publication, this yet to be published report will be the appropriate citation. Additionally we recommend to state the version used, e.g. v2018a or a GIT revision / tag.

## Cite VA
**Application**

If you want to cite the VA application, please refer to the [Zenodo](https://doi.org/10.5281/zenodo.13680258){target="_blank"} entry of the VA version you are using.

**Website**

If you want to refer to this website, you can use this text (adjust the access date)

>*Institute for Hearing Technology and Acoustics, RWTH Aachen University*<br>
>Virtual Acoustics Website<br>
>*https://www.virtualacoustics.org (Accessed on 2020-04-21)*

or the BibTex code
```
@MISC{ihta2020va,
  author = {{Institute for Hearing Technology and Acoustics, RWTH Aachen University}},
  title = {{Virtual Acoustics -- A real-time auralization framework for scientific research}},
  howpublished = {\url{http://www.virtualacoustics.org/}},
  note = {Accessed on 2020-04-21},
  keywords = {auralization,real-time signal processing,virtual reality}
}
```

## VA system publications

*F. Wefers, S. Pelzer, R. Bomhardt, M. Müller-Trapet and M. Vorländer*<br>
**Audiotechnik des aixCAVE Virtual Reality-Systems**<br>
DAGA **2015**, Nürnberg (Germany)

 *D. Schröder, F. Wefers, S. Pelzer and M. Vorländer*<br>
**Acoustic Virtual Reality at RWTH Aachen University**<br>
1st EAA EuroRegio 2010, Congress on Sound and Vibration, **2010**, Ljubljana, Slovenia

*D. Schröder, F. Wefers, S. Pelzer, D. Rausch, M. Vorländer and T. Kuhlen*<br>
**Virtual reality system at RWTH Aachen University**<br>
Proceedings of the International Symposium on Room Acoustics (ISRA), Melbourne, Australia, **2010**

*T. Lentz, D. Schröder, M. Vorländer and I. Assenmacher*<br>
**Virtual reality system with integrated sound field simulation and reproduction**<br>
EURASIP J. Appl. Signal Process., Hindawi Publishing Corp., **2007**

*I. Assenmacher, T. Kuhlen, T. Lentz and M. Vorländer*<br>
**Integrating real-time binaural acoustics into VR applications**<br>
Proceedings of the Tenth Eurographics conference on Virtual Environments, **2004**


## VA technology publications

*J. Stienen and M. Vorländer*<br>
**Real-time auralization of propagation paths with reflection, diffraction and the Doppler shift**<br>
DAGA **2018**, München (Germany)

*F. Wefers and M. Vorländer*<br>
**Flexible data structures for dynamic virtual auditory scenes**<br>
Virtual Acoustics, Springer, **2018**

*J. Mecking, P. Schäfer, J. Stienen and M. Vorländer*<br>
**Efficient simulation of sound paths in the atmosphere**<br>
Proc. Internoise, Hong Kong, **2017**

*F. Wefers*<br>
**Partitioned convolution algorithms for real-time auralization**<br>
RWTH Aachen University, **2015**

*F. Wefers and M. Vorländer*<br>
**Strategies for the real-time auralization of fast moving sound sources in interactive virtual environments**<br>
Internoise **2015**, San Francisco, USA

*F. Wefers and M. Vorländer*<br>
**Bewegungsprädiktion in der Echtzeit-Auralisierung dynamischer Schallfelder**<br>
DAGA **2015**, Nürnberg (Germany)

*J.-G. Richter, M. Pollow, F. Wefers and J. Fels*<br>
**Spherical harmonics based HRTF datasets: Implementation and evaluation for real-time auralization**<br>
Acta acustica united with Acustica, **2014**

*F. Wefers and M. Vorländer*<br>
**Zeitvariante Beschreibung virtueller Szenen für die Echtzeit-Auralisierung instationärer Schallfelder**<br>
DAGA **2014**, Oldenburg (Germany)

*F. Wefers, J. Stienen, S. Pelzer and M. Vorländer*<br>
**Interactive Acoustic Virtual Environments using Distributed Room Acoustics Simulation**<br>
Proc. of the EAA Joint Symposium on Auralization and Ambisonics, Berlin, Germany, **2014**

*S. Pelzer, L. Aspöck, D. Schröder and M. Vorländer*<br>
**Integrating Real-Time Room Acoustics Simulation into a CAD Modeling Software to Enhance the Architectural Design Process**<br>
Buildings, MDPI, **2014**

*B. Masiero and M. Vorländer*<br>
**A Framework for the Calculation of Dynamic Crosstalk Cancellation Filters**<br>
Audio, Speech, and Language Processing, IEEE/ACM Transactions, **2014**

*F. Wefers and M. Vorländer*<br>
**Optimal Filter Partitions for Non-Uniformly Partitioned Convolution**<br>
Audio Engineering Society Conference: 45th International Conference: Applications of Time-Frequency Processing in Audio, **2012**

*D. Schröder*<br>
**Physically Based Real-Time Auralization of Interactive Virtual Environments**<br>
RWTH Aachen University, **2011**

*F. Wefers*
**OpenDAFF - a free open-source software package for directional audio data**<br>
36. Deutsche Jahrestagung für Akustik (DAGA 2010), Berlin, Germany, **2010**

*F. Wefers and D. Schröder*<br>
**Real-Time Auralization of Coupled Rooms**<br>
Proceedings of the EAA Symposium on Auralization, Espoo, Finland, **2009**

*F. Wefers and D. Schröder*<br>
**Real-time filtering structures for interactive geometry modification in acoustic virtual reality**<br>
Proceedings / NAG/DAGA 2009, International Conference on Acoustics : Rotterdam (NL), **2009**

*M. Vorländer*<br>
**Auralization: Fundamentals of Acoustics, Modelling, Simulation, Algorithms and Acoustic Virtual Reality**<br>
Springer, **2007**


## Applied VA publications

*F. Pausch, G. Behler and J. Fels*<br>
**SCaLAr - A surrounding spherical cap loudspeaker array for flexible generation and evaluation of virtual acoustic environments**<br>
Acta Acustica, 4(5):19, **2020**, DOI: [10.1051/aacus/2020014](https://doi.org/10.1051/aacus/2020014){target="_blank"}.

*F. Pausch and J. Fels*<br>
**Localization Performance in a Binaural Real-Time Auralization System Extended to Research Hearing Aids**<br>
Trends in Hearing, vol. 24, pp. 1–18, **2020**, DOI: [10.1177/2331216520908704](https://doi.org/10.1177/2331216520908704){target="_blank"}.

*F. Pausch and J. Fels*<br>
**MobiLab - A Mobile Laboratory for On-Site Listening Experiments in Virtual Acoustic Environments**<br>
Acta Acustica united with Acustica, 105(5), pp. 875-887, **2019**, DOI: [10.3813/AAA.919367](https://doi.org/10.3813/AAA.919367){target="_blank"}.

*F. Pausch, L. Aspöck, M. Vorländer and J. Fels*<br>
**An Extended Binaural Real-Time Auralization System With an Interface to Research Hearing Aids for Experiments on Subjects With Hearing Loss**<br>
Trends in Hearing, vol. 22, pp. 1–32, **2018**, DOI: [10.1177/462331216518800871](https://doi.org/10.1177/2331216518800871){target="_blank"}.

*J. Mecking and M. Vorländer*<br>
**Einfluss von Wettermodellen auf die Auralisierung von Flugzeugen**<br>
DAGA **2018**, München (Germany)

*J. Oberem and J. Fels*<br>
**Experiments on localization accuracy with non-individual and individual HRTFs comparing static and dynamic reproduction methods**<br>
DAGA **2018**, München (Germany)

*R. A. Viveros Munoz and J. Fels*<br>
**Assessment of individual head-related transfer function and undirected head movements of normal listeners in a moving speech-in-noise task using virtual acoustics**<br>
DAGA **2018**, München (Germany)

*F. Pausch and J. Fels*<br>
**Auditory distance perception in virtual acoustic environments using binaural technology and simulated room acoustics**<br>
DAGA **2017**, Kiel (Germany)

*R. A. Viveros Munoz and J. Fels*<br>
**The benefit of head movements of normal listeners in a dynamic speech-in-noise task with virtual acoustics**<br>
DAGA **2017**, Kiel (Germany)

*J. Tumbrägel, F. Pausch, L. Aspöck and J. Fels*<br>
**Investigations on binaural reproduction in virtual acoustic environments using hearing aids**<br>
DAGA **2016**, Aachen (Germany)

*F. Pausch, Z. E. Peng, L. Aspöck and J. Fels*<br>
**Speech perception by children in a real-time virtual acoustic environment with simulated hearing aids and room acoustics**<br>
ICA **2016**, Buenos Aires (Argentina)

*F. Pausch, M. Kohnen, L. Aspöck, Z. E. Peng and Janina Fels*<br>
**Investigation of sound localization performance in a virtual acoustic environment designed for hearing aid users**<br>
DAGA **2016**, Aachen (Germany)

*Z. E. Peng, F. Pausch and Janina Fels*<br>
**Auditory Training of Spatial Processing in Children with Hearing Loss in Virtual Acoustic Environments: Pretest Results**<br>
DAGA **2016**, Aachen (Germany)

*R. A. Viveros Munoz, Z. E. Peng, F. Pausch and Janina Fels*<br>
**Eﬀect of a moving distractor on speech intelligibility in babble noise using a digit-triplet test**<br>
DAGA *2016*, Aachen (Germany)

*A. Sahai, F. Wefers, S. Pick, E. Stumpf, M. Vorländer and T. Kuhlen*<br>
**Interactive simulation of aircraft noise in aural and visual virtual environments**<br>
Applied Acoustics, **2016**

*M. Vorländer, D. Schröder, S. Pelzer and F. Wefers*<br>
**Virtual reality for architectural acoustics**<br>
Journal of Building Performance Simulation, **2015**

*F. Pausch, L. Aspöck and J. Fels*<br>
**Evaluation of binaural sound reproduction systems**<br>
DAGA **2015**, Nuremberg (Germany)

*L. Aspöck, F. Pausch, M. Vorländer and J. Fels*<br>
**Dynamic real-time auralization for experiments on the perception of hearing impaired subjects**<br>
DAGA **2015**, Nuremberg (Germany)

*A. Sahai, E. Anton, E. Stumpf, F. Wefers and M. Vorländer, M.*<br>
**Interdisciplinary Auralization of Take-off and Landing Procedures for Subjective Assessment in Virtual Reality Environments**<br>
18th AIAA/CEAS Aeroacoustics Conference (33rd AIAA Aeroacoustics Conference), **2012**

*F. Wefers and M. Vorländer*<br>
**VATSS : ein Virtual Reality System zur interaktiven Simulation von Fluglärm**<br>
DAGA **2012**, Darmstadt (Germany)