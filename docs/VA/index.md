---
template: special_content.html
title: Welcome to VA
hide:
  - navigation
  - toc
---

<div markdown class="grid-container">
<div markdown>
# Welcome to Virtual Acoustics {style="text-align:end;"}

Virtual Acoustics (VA) is a **real-time auralization framework** for scientific research providing modules and interfaces for experiments and demonstrations. It is open source and fully controllable enabling *reproducible research*.
{style="text-align:end;"}
</div>
<div markdown class="grid-container__side">
![VR Teaser](../assets/vr_teaser.jpg){ loading=lazy width=70% class="image-circ-cropper" }
</div>
</div>

-------------

## What is Virtual Acoustics?

VA is a software that was created at the Institute for Hearing Technology and Acoustics (IHTA) at RWTH Aachen University in Germany over a decade ago as an auralization application - one of the first of its kind.

Over the years, VA has been reinvented and improved by dint of several contributions in the scope of Bachelor, Master and PhD theses to meet state-of-the-art standards in audio rendering and spatial audio reproduction.

:VA:{ style="font-size:10em;stroke: var(--md-typeset-color);fill: var(--md-typeset-color);" }

-------------

## Is Virtual Acoustics suitable for me?

Until now, VA is used for scientific research purposes at IHTA.
It has been designed to meet high quality audio rendering & reproduction demands for listening experiments as well as Virtual Reality research.

Previously unreleased, VA will be continued as an open source project and can be used for a variety of applications.

VA is made for Windows, but can also be used on other platforms.

-------------

## Navigation

<div markdown class="grid-container" style="align-items: start;">
<div markdown>
:fontawesome-solid-lightbulb:{ style="font-size:5em" }

[Overview](overview.md){ .md-button }

About VA in a nutshell.
</div>
<div markdown>
:fontawesome-solid-cloud-arrow-down:{ style="font-size:5em" }

[Downloads](download.md){ .md-button }

Find your package in the downloads section.
</div>
<div markdown>
:fontawesome-solid-book:{ style="font-size:5em" }

[Documentation](documentation/index.md){ .md-button }

Learn how to work with VA: setup, configuration, virtual scenes.
</div>
<div markdown>
:fontawesome-solid-graduation-cap:{ style="font-size:5em" }

[Research](research.md){ .md-button }

Get a list of publications about VA and its applications in science.
</div>
<div markdown>
:fontawesome-solid-book:{ style="font-size:5em" }

[Support](support.md){ .md-button }

For FAQs, Issue tracking and more.
</div>
<div markdown>
:fontawesome-solid-flask:{ style="font-size:5em" }

[Developer section](../developer.md){ .md-button }

Get more information on how you can build VA for your platform.
</div>
</div>