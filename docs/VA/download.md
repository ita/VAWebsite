---
Title: Download
---

# Download section {style="text-align: center;"}

Get Virtual Acoustics (VA) for Windows
{style="font-size:1.2em;text-align: center;"}
<hr class="accent-hr">

## Full VA package for Windows {style="text-align: center;"}

<div markdown class="grid-container" style="align-items: end;text-align:center">
[![VA for Windows](../assets/VA_Redstart_for_windows.png){ loading=lazy width=40% style="border-radius: .2rem;" }](https://doi.org/10.5281/zenodo.13788752)
</div>

**Build name**: [VA_full.v2024a.win64.vc14](https://doi.org/10.5281/zenodo.13788752){target="_blank"}<br>
*Includes VAServer, VC14 C++ programming libraries, all bindings and Unity scripts.*

**Quick user guide**:<br>
A quick guide can be found in the [documentation](documentation/index.md#quick-start-guide) section.

**Binary package license / sharing**:<br>
GNU General Public License v3.0 or later

**Details**:<br>
+ VABase, VANet and VACore* C++ developer packages for Visual C++ Compiler Version 14 (Visual Studio 2019)<br>
+ VAMatlab and VAC# bindings<br>
+ VAPython binding wheel valid for Python >= 3.8<br>
+ VAUnity<br>
+ VAServer command line interface application<br>
- Redstart VA GUI application [DEPRECATED]<br>

*includes FFTW3 (GNU GPL)
{style="font-size:0.6em;"}

----------

## Referencing via DOI
All official VA versions are now published via [Zenodo](https://doi.org/10.5281/zenodo.13680258){target="_blank"} and therefore can be referenced using a digital object identifier (DOI).

----------

## Important changes
**2023 & 2024**

In 2023 and 2024, respectively, the network communication underwent two major reworks. As a result, there is no backwards compatibility between respective versions as well as to previous versions regarding the network communication. For example, controlling a `VAServer` of version 2024a with `VAMatlab` v2023a will not work. The same holds for version 2023a and 2022a.

**2022**

In 2022, we introduced major changes in our build system. This allows us to use compilers newer than VC12 for all VA components. Generally, this lead to the following changes for newly deployed VA versions:

- VAPython is now part of the `VA_full` build. The `VA_bindings` build is not shipped anymore.
- VAPython is now included as *wheels* which makes installing much easier
- We stopped developing the Redstart GUI and removed it from the packages

----------

## Changelog

??? note "v2024a"
    **Sound-path-based renderers**

    - Now allows changing the default reflection factor in VACore.ini
    - Enhancement of gain fading between audio blocks
        - Now uses cosine-squared instead of linear fading
        - Now also works with Ambisonics spatialization
    
    **VANet**

    - Cleanup: removed irrelevant/unusable interface functions, which raised a 'not-implemented' error.

    **VAPython**

    - Got reworked as pure Python package (no C++ bindings anymore)
    - Now has `NatNetTracking` support
    - See detailed changelog on [git](https://git.rwth-aachen.de/ita/VAPython/-/blob/master/CHANGELOG.md?ref_type=heads#2024a---2024-09-26){target="_blank"}

??? note "v2023b"
    **General**

    - Now default paths (`data` and `conf`) are automatically added and do not need to be added to `VACore.ini`
    - The `data` folder now contains additional signals and directivities (e.g. for car pass-by and aircraft noise).

    **AirtrafficNoiseRenderer**

    - Now allows real-time auralization with inhomogeneous medium
    - Data histories for simulation results can now be configured to select interpolation method

    **VAMatlab**

    - Significant rework/extension of examples: Now most examples
        - ...come with a VACore.ini file
        - ...automatically start the VAServer for you
        - ...have detailed comments and cross-references

    **Bugfixes**

    - General
        - Fixed several bugs regarding the calculation of air attenuation: in some cases attenuation values could be significantly off
        - The `lock_scene()` and `unlock_scene()` did not work anymore due to change of VANet in v2023a, which is fixed now
    - VAServer
        - Now properly cleans up the core if closing the window
        - `run_VAServer_recording.bat` now works again: fixed start argument for remote control

??? note "v2023a"
    **Network communication rework (VANet)**

    - Now based on [gRPC](https://grpc.io/){target="_blank"}
    - No backwards compatibility (e.g. `VAMatlab v2022a` won't work with `VAServer v2023a`)

    **General**

    - New thread-safe logging system with improved formatting and file log
    - VAServer: Improved [command line interface](./documentation/vaserver.md#advanced-command-line-interface), with backwards compatibility
    - Now all coord transformations are unified throughout VAServer and bindings

    **Rendering (VACore)**

    - Sound-path-based renderers now use one SIMO VDL per source instead of one VDL per sound path
    - AirTrafficNoiseRenderer now allows scheduling of [ART](../GA/art.md) simulations for real-time purposes
    - New OutdoorNoiseRenderer which allows binaural, ambisonics and VBAP signals (replaces BinauralOutdoorNoiseRenderer)

    **VAMatlab**

    - New setup function (`VAMatlab_setup()`) adding relevant folders to Matlab path
    - Now has a set of convenience classes to render dynamic scenarios
    - Supports multiple tracked sources

    **VAPython**

    - Now comes with a single, version independent wheel for Python >= 3.8
    - Fixed a bunch of bugs leading to increased robustness

    **Bugfixes**

    - Fixed a bug where third-octave band magnitude spectra were not parsed correctly when calling `SetParameters()`
    - Fixed a crash that could occur when using the `Burg` algorithm for IIR filtering
    - See VAPython

??? note "v2022a"
    **VACore - Rendering system rework**

    - Now distinguishs between FIR-based and sound-path-based renderers
    - Sound-path-based renderers are not restricted to binaural output anymore but can choose from binaural / Ambisonics / VBAP
    - e.g. `FreeField` renderer replaces `AmbisonicsFreeField` and `VBAPFreeField`

    **VAMatlab**

    - Tracking now works with AR-Tracking (in addition to OptiTrack)

    **VAPython**
    
    - Is now installed using *wheels*
    - Now shipped with version 3.8 to 3.10

    **Bugfixes**

    - Undesired default receiver position (`[0 0 0]`) removed
    - Fixed performance issues of Windows timer used in *VAMatlab*
    - Fixed CTC crash that could occur if utilized HRTF did not cover a full sphere
    - Fixed bug in *ITASimulationScheduler* where mainly a single source-receiver pair was simulated disregarding other sound sources

??? note "v2021a"
    **General**

    - Now a receiver becomes active as soon as it has a valid position. The `set_active_listener` function should not be used anymore was removed from all bindings.

    **VAServer / VACore**

    - Now comes with `VACore.recording.ini` file for offline rendering. Use `run_VAServer_recording.bat` to start VAServer with this configuration.

    **VACore - AirtrafficNoiseRenderer**

    - Now can switch between homogeneous and inhomogeneous medium
    - Homogeneous medium: straight sound paths calculated within VA
    - Inhomogeneous medium: curved sound paths simulated with [Atmospheric Ray Tracing](../GA/art.md) framework

    **VAPython**

    - Previously only working for Python 3.6, now only works with Python 3.7 and higher
    - Now shipped with version 3.7 to 3.9

----------

## Snapshots

The following builds include the VAServer application as well as all bindings.
They are distributed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html){target="_blank"} or later.
>*Note: Versions prior to 2022a do not include VAPython. See __historic binding-only builds__.*

| Version | Build name                    | DOI                                                               |
| ------- | ----------------------------- | ----------------------------------------------------------------- |
| 2024a   | VA_full.v2024a.win64.vc14     | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13788752.svg)](https://doi.org/10.5281/zenodo.13788752){target="_blank"} |
| 2023b   | VA_full.v2023b.win64.vc14     | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13744554.svg)](https://doi.org/10.5281/zenodo.13744554){target="_blank"} |
| 2023a   | VA_full.v2023a.win64.vc14     | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13744544.svg)](https://doi.org/10.5281/zenodo.13744544){target="_blank"} |
| 2022a   | VA_full.v2022a.win64.vc14     | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13744523.svg)](https://doi.org/10.5281/zenodo.13744523){target="_blank"} |
| 2021a   | VA_full.v2021a.win32-x64.vc12 | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13744493.svg)](https://doi.org/10.5281/zenodo.13744493){target="_blank"} |
| 2020a   | VA_full.v2020a.win32-x64.vc12 | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13744474.svg)](https://doi.org/10.5281/zenodo.13744474){target="_blank"} |
| 2019a   | VA_full.v2019a.win32-x64.vc12 | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13744446.svg)](https://doi.org/10.5281/zenodo.13744446){target="_blank"} |
| 2018b   | VA_full.v2018b.win32-x64.vc12 | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13744252.svg)](https://doi.org/10.5281/zenodo.13744252){target="_blank"} |
| 2018a   | VA_full.v2018a.win32-x64.vc12 | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13680259.svg)](https://doi.org/10.5281/zenodo.13680259){target="_blank"} |

**Historic binding-only builds**

The following list include static builds of VA's bindings (excluding the VAServer application).
They are distributed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0){target="_blank"}.
>*Note: Binding-only builds are not published anymore, after version 2021a*

| Details                  | Build name                                                                                                             |
|--------------------------|------------------------------------------------------------------------------------------------------------------------|
| v2021a bindings (static) | [VA_bindings.v2021a.win32-x64.vc14_static](https://rwth-aachen.sciebo.de/s/qOIpg23cAnZDiE3/download){target="_blank"}  |
| v2020a bindings (static) | [VA_bindings.v2020a.win32-x64.vc14_static](https://rwth-aachen.sciebo.de/s/unWOHSO72yJyxog/download){target="_blank"}  |
| v2019a bindings (static) | [VA_bindings.v2019a.win32-x64.vc14_static](https://rwth-aachen.sciebo.de/s/Rfx4Mj9RhO3WnSa/download){target="_blank"}  |
| v2018b bindings (static) | [VA_bindings.v2018b.win32-x64.vc14_static](https://rwth-aachen.sciebo.de/s/AovWyvDMGWWIaef/download){target="_blank"}  |
| v2018a bindings (static) | [VA_bindings.v2018a.win32-x64.vc14_static](https://rwth-aachen.sciebo.de/s/uW4dRhjm5FKA0yZ/download){target="_blank"}  |

----------

## Linux, Mac OSX and other

From the very beginning, Virtual Acoustics has been developed and used under **Windows** platforms. If you do not have a Windows system, you can still use VA. Unfortunately, we are not experienced in building binary packages for other platforms than Windows, hence you will have to build it from the source code. However, we can't guarantee smooth performance on those systems. For more information, read the [developer section](../developer.md).

A common alternative way is to run the VA server application on a dedicated remote Windows PC with a proper ASIO interface, and control it from any other platform. This way, you can use the Windows download package and you only have to build the remote control part of VA (the C++ interface libraries and/or bindings). You will require no further third-party libraries except for the binding interface you choose (usually Matlab or Python). You can also run a Jupyter notebook along with VA on the Windows machine and remotely control everything using a web browser on any mobile device connected to the same network. 

----------

## Some hints on VA packages

VA is always in development and we constantly add new features or components. Therefore, we chose the version identifier to reflect the release year and added an ascending alphabetic character, like `v2018a`. This makes it easier to determine the release time, just like in Matlab and other applications.

We **cannot guarantee compatibility among VA versions!** The reason is, that we still update the interface for new powerful features. This is unfortunate, but can not be achieved with the resources we can afford. If you are interested in new features, you will have to pay the price and update everything, including the bindings you use.

We adopt naming conventions for platforms and compiler versions from [ViSTA VR Toolkit](https://www.itc.rwth-aachen.de/cms/IT-Center/Forschung-Projekte/Virtuelle-Realitaet/Infrastruktur/~fgmo/ViSTA-Virtual-Reality-Toolkit/){target="_blank"}. This way, one can easily extract the target platform, such as `win32-x64` for a Windows operating system using a mixed 32 bit and 64 bit environment, or `vc12` to indicate that the binary package was built using the Microsoft Visual C++ Compiler Version 12. If you are missing redistributable libraries you can identify the required Microsoft installer by this suffix.

The build environment of VA provides configuration of the underlying components. This may or may not result in a binary package that links against GNU GPL licensed libraries. For this reason, VA is either licenses under the favored Apache License, Version 2.0 or under the GNU General Public License, as required by the copyleft nature. 