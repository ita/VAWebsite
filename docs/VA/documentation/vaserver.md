# VAServer app

The VAServer is the application which performs the actual auralization. It can be configured using [configuration files](configuration.md) prior to the startup and [controlled](control.md) via bindings during runtime.

The easiest way to start the VAServer is to use the provided batch scripts. For example, `run_VAServer.bat` will start a local server using the default configuration file `bin/VACore.ini`. It is **highly recommended** to start the `VAServer` from the VA root folder, since this allows VA to get access to provided configuration and data files. This is the reason why the provided batch files are located within the root folder.

You can create your own batch files linking to user-defined INI files to configure VA to your needs. Similarly you can execute the respective commands using the command line directly.

----------

## Basic command line calls

Launching VA from the command line allows specifying a server address / port plus a config file (`VACore.ini`).
```cmd
VASERVER_PATH/VAServer.exe SERVER_ADDRESS:PORT VACORE_INI_PATH
```

Opening the command line in VA's main folder, this would be a valid example:
```cmd
bin/VAServer.exe localhost:12340 conf/VACore.ini
```

>Note, that the VAServer will parse paths relative to the current working directory.

## Advanced command line interface

VAServer (starting from version 2023a) provides a more powerful command line interface which has the typical features. For example, the VAServer version can be printed using:

```cmd
VAServer.exe --version
```

Most importantly it allows printing a help message listing arguments and options. Calling

```cmd
VAServer.exe --help
```

will print a help message similar to this:

```cmd

  VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
   VVV      VVV AAA
    VVV    VVV   AAA
     VVV  VVV     AAA        Copyright 2015-2023
      VVVVVV       AAA       Institute for Hearing Technology and Acoustics (IHTA)
       VVVV         AAA      RWTH Aachen University

VAServer is a lightweight command line C++ server application for real-time auralization.
The configuration of the application is done in an '.ini' file which can be found under the 'conf' folder.

For more information, especially for all configuration options visit https://www.virtualacoustics.de/VA/documentation/ .

Usage: VAServer [OPTIONS] [server-address] [config_file]

Positionals:
  server-address ADDR:PORT=0.0.0.0:12340
                              Address of the server, in the from (address):(port)
  config_file FILE=conf/VACore.ini
                              Core configuration ini file

Options:
  -h,--help                   Print this help message and exit
  -v,--version                Display program version information and exit
```

> Again note, that the VAServer will parse paths relative to the current working directory.
