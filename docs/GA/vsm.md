---
title: Virtual source method
---

# Virtual source method {style="text-align: center;"}

An interface to link between atmospheric and urban sound propagation models
{style="font-size:1.2em;text-align: center;"}

<hr class="accent-hr">

## Overview

The virtual source method allows to link between models for curved sound propagation in an inhomogeneous, moving atmosphere and urban sound propagation considering reflections and diffraction and urban structure. It allows running a combined simulation with the goal of creating auralizations considering the propagation effects of both simulation domains. This is of particular interest for the auralization of aircraft flyovers close to residential areas.

![type:video](https://www.youtube.com/embed/jNWJOKI9sTc)

_Aircraft flyover auralization based on the virtual source method_
{style="text-align: center;"}

As interface, a virtual source position is calculated based on a curved free-field path considering the atmospheric effects. Using this position for the urban simulation allows maintaining important acoustic properties, such as incoming direction and propagation delay. Details of this approach are well documented in this open-access [publication](https://doi.org/10.1051/aacus/2022021){target="_blank"}.

The virtual source method is [implemented](#matlab-implementation) in Matlab allowing to combine our [Atmospheric Ray Tracing](art.md) framework and the [image edge model](iem.md). Being part of the [ITA-Toolbox](https://www.ita-toolbox.org/){target="_blank"}, the respective code is open-source.

------

## Matlab implementation {style="text-align: center;"}

Linking between the ART framework and the image edge model
{style="text-align: center;"}

<hr class="accent-hr">


The virtual source method is implemented in Matlab as part of the [ITA-Toolbox](https://www.ita-toolbox.org/){target="_blank"}.
It allows to link between the [Atmospheric Ray Tracing](art.md) framework and the  [image edge model](iem.md). The overall simulation chain then considers curved sound propagation caused by an inhomogeneous atmosphere as well as higher order reflections and diffraction at urban structures.

### Requirements

The [ITA-Toolbox](https://www.ita-toolbox.org/){target="_blank"} for Matlab must be installed.
Additionally, the [ARTMatlab](art.md#download) and [pigeon](iem.md#download) app must be downloaded and added to the Matlab search path.

### Matlab classes

|Class name          |Description|
|:-------------------|:----------|
|`itaCauSimulation`  | Class to run a combined simulation leading to a set of sound paths considering atmospheric and urban propagation effects. |
|`itaCauPropagation` | Class to derive acoustic parameters from those sound paths which can be used to auralize a scenario. |

The respective class files can be found in the `applications/SoundFieldSimulation/CombinedAtmosphereUrban` subfolder of the ITA-Toolbox.

### Tutorials

Check out the example scripts provided in the `applications/SoundFieldSimulation/CombinedAtmosphereUrban/examples` folder of the ITA-Toolbox:

- Sound path simulation of a static scenario: `ita_cau_simulation_example.m`
- Auralization of an aircraft flyover: `ita_cau_auralization_example.m`


------

## Scientific publications {style="text-align: center;"}

<hr class="accent-hr">

_P. Schäfer, Lennart Reich and M. Vorländer_<br>
___Linking atmospheric and urban auralization models___<br>
Acta Acustica, Volume 5, 2021<br>
[https://doi.org/10.1051/aacus/2022021](https://doi.org/10.1051/aacus/2022021){target="_blank"}
