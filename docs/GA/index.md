---
title: Overview
hide: 
  - toc
  - navigation
---

# ITAGeometricalAcoustics {style="text-align: center;"}

The open-source C++ library collection for the simulation of **outdoor sound propagation** using the principle of **geometrical acoustics**
{style="font-size:1.2em;text-align: center;"}

<hr class="accent-hr">

<div markdown class="grid-container" style="align-items: end;text-align:center">
<div markdown>
## Atmospheric sound propagation

[![Aircraft above IHTA with sound paths](../assets/aircraft_above_ihta_with_sound_paths-480.jpg){ srcset="../assets/aircraft_above_ihta_with_sound_paths-480.jpg 480w, ../assets/aircraft_above_ihta_with_sound_paths-960.jpg 960w, ../assets/aircraft_above_ihta_with_sound_paths-1920.jpg 1920w" loading=lazy  width=100% style="border-radius: .2rem;" }](art.md)

[Atmospheric Ray Tracing](art.md){ .md-button .md-button--primary}
</div>
<div markdown>
## Urban sound propagation

[![Urban scene with sound paths](../assets/urban_scene_with_sound_paths-480.jpg){ srcset="../assets/urban_scene_with_sound_paths-480.jpg 480w, ../assets/urban_scene_with_sound_paths-960.jpg 960w, ../assets/urban_scene_with_sound_paths-1920.jpg 1920w" loading=lazy width=100% style="border-radius: .2rem;" }](iem.md)

[Image edge model](iem.md){ .md-button .md-button--primary}
</div>
<div markdown>
## Combined sound propagation

[![Aircraft above urban area with sound paths](../assets/aircraft_above_urban_area_with_sound_paths-480.jpg){ srcset="../assets/aircraft_above_urban_area_with_sound_paths-480.jpg 480w, ../assets/aircraft_above_urban_area_with_sound_paths-960.jpg 960w, ../assets/aircraft_above_urban_area_with_sound_paths-1920.jpg 1920w" loading=lazy width=100% style="border-radius: .2rem;" }](vsm.md)

[Virtual source method](vsm.md){ .md-button .md-button--primary}
</div>
</div>
