# Copyright
Copyright 2017-present Institute for Hearing Technology and Acoustics (IHTA), RWTH Aachen University

# License
Creative Commons Attribution 4.0 (CC BY 4.0)
https://creativecommons.org/licenses/by/4.0/
